import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
/*Pages*/
import { HomePage } from '../pages/home/home';
import {LoginPage} from "../pages/index.paginas";
/*Plugins*/
import {Storage} from "@ionic/storage";
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, storage:Storage) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

        storage.get('loginState').then((value)=>{
            console.log(value);
            splashScreen.hide();
            if(value == true) {
                //Comprobar si es la primera vez que abre la app
                this.rootPage = HomePage;
                splashScreen.hide();
            }else{
                this.rootPage = LoginPage;
                splashScreen.hide();
            }
        }).catch(()=>{
            this.rootPage = LoginPage;
            splashScreen.hide();
        })
    });
      storage.get('loginState').then((val) => {
          console.log('Your age is', val);
      });
      statusBar.styleDefault();
      splashScreen.hide();
  }
}

